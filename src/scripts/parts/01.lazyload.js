$('.lazy').Lazy({
	afterLoad: function (element) {
		// console.log(element);
		element.addClass('lazy-loaded');
	}
});


// Lazy load for picture tag
$('.lazy-picture').Lazy({
	threshold: 0,
	afterLoad: function (element) {
		// console.log(element);
		element.addClass('lazy-picture-loaded');
	}
});