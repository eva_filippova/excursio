(function() {
	var $countHolder = $('#people-count-result'),
		$peopleCount = $('#people-count'),
		$inputHolder = $('#people-count-wrap');

	$countHolder.click(function() {
		$peopleCount.toggleClass('open');
		$inputHolder.toggleClass('open');

		if ($peopleCount.hasClass('open')) {

			$('body').click('click.people', function(e) {
				var $target = $(e.target);

				if ($target.closest('#people-count-wrap').length < 1) {
					$peopleCount.removeClass('open');
					$inputHolder.removeClass('open');
					$('body').off('click.people');
				}
			});
		}
	});

	$countHolder.on('keyup', function(e) {
		if (e.which == 9) {
			$peopleCount.addClass('open');
			$inputHolder.addClass('open');
		}

		if ($peopleCount.hasClass('open')) {

			$('body').click('click.people', function(e) {
				var $target = $(e.target);

				if ($target.closest('#people-count-wrap').length < 1) {
					$peopleCount.removeClass('open');
					$inputHolder.removeClass('open');
					$('body').off('click.people');
				}
			});
		}
	});


})();
