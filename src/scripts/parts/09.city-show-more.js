(function() {
	var $link = $('#popular-cities-show-more'),
		$list = $('#popular-cities-list'),
		$item = $('.js-city-item'),
		textHide = $link.data('text-hide'),
		textShow = $link.data('text-show'),
		ww = $(window).width(),
		max;

	if (ww < 768) {
		max = 1
	}
	else if (ww < 1024) {
		max = 3
	}
	else {
		max = $item.length;
	}

	$('.js-city-item:gt('+max+')').hide().end();
	$list.addClass('hide');

	$link.click(function() {
		var isHide = $list.hasClass('hide'),
			$body = $('html, body'),
			offset = $list.offset().top;

		$('.js-city-item:gt('+max+')').toggle();
		$list.toggleClass('hide');


		if (!isHide) {
			$link.text(textShow);

			$body.animate({
				scrollTop: offset
			}, 300);
		}
		else {
			$link.text(textHide);
		}

		return false;
	});

})();