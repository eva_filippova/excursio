(function() {
	var $input = $('#top-filter-datepicker'),
		$inputHolder = $('#top-filter-datepicker-wrap');

	function setDPPosition() {
		var inputWidth = $input.outerWidth(),
			ww = $(window).width(),
			$datepicker = $('.datepicker');

		if (ww < 768) {
			$datepicker.css('width', inputWidth);
		}
	}

	// Initialization
	$('#top-filter-datepicker').datepicker({
		// inline: true,
		position: $(window).width() >= 768 ? "bottom center" : "bottom left",
		dateFormat: "d MM yyyy",
		onShow: function(dp, calendarVisible) {
			var inputWidth = $input.outerWidth(),
				ww = $(window).width(),
				$datepicker = $('.datepicker');

			$inputHolder.addClass('open');

			$('#top-filter-datepicker').one('click.hide', function() {
				if (calendarVisible === true) {
					var datepicker =  $('#top-filter-datepicker').data('datepicker');
					datepicker.hide();
					calendarVisible = false;
				}
			});

			setTimeout(function() {
				calendarVisible = true;
			}, 200);

			if (ww < 768) {
				$datepicker.css('width', inputWidth);
			}
		},
		onHide: function() {
			$inputHolder.removeClass('open');
			$('#top-filter-datepicker').off('click.hide');
		},
		onChangeMonth: setDPPosition,
		onChangeYear: setDPPosition,
		onChangeView: setDPPosition,
		onChangeDecade: setDPPosition
	});



	// console.logisVisible);

	// Access instance of plugin
	// $('#top-filter-datepicker').data('datepicker');

	$('.datepicker--content').append('<div class="datepicker--notice">Выберите одну или несколько дат, которые Вам подходят</div>');

})();