(function() {
	var $burger = $('#header-burger'),
		$menu = $('#mobile-menu'),
		$body = $('body, html');

	$burger.click(function() {
		var $t = $(this);

		$t.toggleClass('open-menu');
		$menu.toggleClass('open');
		$body.toggleClass('open-menu');

		return false;
	});
})();