(function() {
	var mySwiper = new Swiper ('.swiper-container', {
		direction: 'horizontal',
		loop: true,
		// autoplay: true,
		spaceBetween: 40,
		slidesPerView: 3,
		pagination: {
			el: '.testimonials__content-slider-pagination',
			type: 'bullets',
			clickable: true,
			bulletClass: 'testimonials__content-slider-pagination-item',
			bulletActiveClass: 'testimonials__content-slider-pagination-item_active'
		},
		navigation: {
			prevEl: '.testimonials__content-slider-arrow--prev',
			nextEl: '.testimonials__content-slider-arrow--next',
		},
		breakpoints: {
			768: {
				slidesPerView: 1,
			},
		},
		on: {
			init: function () {
				$('.js-testimonial-desc').trunk8({
					lines: 4,
					fill: '&hellip; <div class="testimonial__desc-link-wrap" id="read-more"><a class="testimonial__desc-link" href="#">Показать полностью</a></div>'
				});

				$(document).on('click', '#read-more', function (event) {
					var $item = $(this).parent().closest('.testimonial');
					$(this).parent().trunk8('revert').append(' <div class="testimonial__desc-link-wrap" id="read-less"><a class="testimonial__desc-link" href="#">Скрыть текст</a></div>');
					$item.addClass('testimonial_open');

					return false;
				});

				$(document).on('click', '#read-less', function (event) {
					var $item = $(this).parent().closest('.testimonial');
					$(this).parent().trunk8();
					$item.removeClass('testimonial_open');
					return false;
				});

				$('.js-testimonial-desc').show().trunk8();
			},
		},
	});
})();
