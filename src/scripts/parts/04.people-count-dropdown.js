'use strict';

(function () {
	var $minusAdults = $('#adults-minus'),
		$plusAdults = $('#adults-plus'),
		$minusChildren = $('#children-minus'),
		$plusChildren = $('#children-plus');

	/**
	 * Функция склоняет руссуие существительные
	 * @param {number} n - число
	 * @param {array<string>} textForms - массив с формами слова (1, 2, 5), например
	 * ['минута', 'минуты', 'минут']
	 */
	var num2str = function num2str(n, textForms) {
		var returnWithNumber = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

		var number = Math.abs(n) % 100;
		var number1 = number % 10;

		var resultForm = void 0;

		if (number > 10 && number < 20) {
			resultForm = textForms[2];
		} else if (number1 > 1 && number1 < 5) {
			resultForm = textForms[1];
		} else if (number1 === 1) {
			resultForm = textForms[0];
		} else {
			resultForm = textForms[2];
		}

		return returnWithNumber ? n + ' ' + resultForm : resultForm;
	};

	var initialState = {
		adults: 1,
		children: 0
	};

	var personsReducer = function personsReducer() {
		var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
		var action = arguments[1];

		switch (action.type) {
		case 'ADULT_INCREMENT':
			return Object.assign({}, state, {
				adults: state.adults + 1
			});

		case 'ADULT_DECREMENT':
			return Object.assign({}, state, {
				adults: state.adults > 1 ? state.adults - 1 : 1
			});

		case 'CHILDREN_INCREMENT':
			return Object.assign({}, state, {
				children: state.children + 1
			});

		case 'CHILDREN_DECREMENT':
			return Object.assign({}, state, {
				children: state.children > 0 ? state.children - 1 : 0
			});
		default:
			return state;
		}
	};

	var store = Redux.createStore(personsReducer);

	function update() {
		var state = store.getState();

		$('#form-adults-count').text(state.adults);
		$('#form-children-count').text(state.children);
		$('#adults-people-count').text(state.adults);
		$('#children-people-count').text(state.children);

		$('#form-adults-text').text(num2str(state.adults, ['взрослый', 'взрослых', 'взрослых']));
		$('#form-children-text').text(num2str(state.children, ['ребенок', 'ребенка', 'детей']));
	}

	store.subscribe(update);

	$minusAdults.click(function () {
		store.dispatch({
			type: 'ADULT_DECREMENT'
		});

		return false;
	});

	$plusAdults.click(function () {
		store.dispatch({
			type: 'ADULT_INCREMENT'
		});

		return false;
	});

	$minusChildren.click(function () {
		store.dispatch({
			type: 'CHILDREN_DECREMENT'
		});

		return false;
	});

	$plusChildren.click(function () {
		store.dispatch({
			type: 'CHILDREN_INCREMENT'
		});

		return false;
	});
})();