(function() {
	var $link = $('.js-currency-trigger');

	$link.click(function() {
		var $t = $(this),
			$dropdown = $t.find('.js-currency-dropdown'),
			$chosenItem = $t.find('.js-currency-chosen'),
			$chosenItemAbbr = $chosenItem.find('.js-currency-chosen-abbr'),
			$chosenItemText = $chosenItem.find('.js-currency-chosen-text'),
			$item = $dropdown.find('.js-currency-item');

		$link.toggleClass('open');
		$dropdown.toggleClass('open');
		$item.click(function() {
			var $t = $(this),
				$abbr = $t.find('.js-currency-item-abbr'),
				abbrText = $abbr.text(),
				$text = $t.find('.js-currency-item-text'),
				textText = $text.text();

			$chosenItemAbbr.text(abbrText);
			$chosenItemText.text(textText);

			$dropdown.removeClass('open');
			$link.removeClass('open');

			return false;
		});

		if ($dropdown.hasClass('open')) {

			$('body').click('click.lang', function(e) {
				var $target = $(e.target);

				if ($target.closest('.js-currency-trigger').length < 1 ) {
					$dropdown.removeClass('open');
					$link.removeClass('open');
					$('body').off('click.lang');
				}
			});
		}
	});
})();