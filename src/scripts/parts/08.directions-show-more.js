(function() {
	var $link = $('#directions-show-more'),
		$list = $('#directions-list'),
		$item = $('.js-directions-item'),
		textHide = $link.data('text-hide'),
		textShow = $link.data('text-show'),
		ww = $(window).width(),
		max;

	if (ww < 768) {
		max = 19
	}
	else if (ww < 1024) {
		max = 29
	}
	else {
		max = $item.length;
	}


	$('.js-directions-item:gt('+max+')').hide().end();
	$list.addClass('hide');

	$link.click(function() {
		var isHide = $list.hasClass('hide'),
			$body = $('html, body'),
			offset = $list.offset().top;

		$('.js-directions-item:gt('+max+')').toggle();
		$list.toggleClass('hide');

		if (!isHide) {
			$link.text(textShow);

			$body.animate({
				scrollTop: offset
			}, 300);
		}
		else {
			$link.text(textHide);
		}

		return false;
	});

})();