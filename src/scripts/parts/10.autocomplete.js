(function() {
	var $searchField = $("#search-input"),
		superUl,
		projects = [
			{
				value: "Барселона",
				label: "",
				category: 'place',
				country: "Испания",
				province: "Каталония",
				excursions: "1 400 экскурсий"
			},
			{
				value: "Батуми",
				label: "",
				category: 'place',
				country: "Грузия",
				province: "Аджария",
				excursions: "730 экскурсий"
			},
			{
				value: "Балаклава",
				label: "",
				category: 'place',
				country: "Крым",
				province: "Севастопольский район",
				excursions: "120 экскурсий"
			}
		];


	$.widget( "custom.catcomplete", $.ui.autocomplete, {
	_create: function() {
		this._super();
		this.widget().menu( "option", "items", "> :not(.ui-autocomplete-category)" );
	},
	_renderItem: function( ul, item ) {
		var regExp = new RegExp(this.term, 'i'),
			newValue = item.value.replace(regExp, "<span class='search__suggestion-highlight'>" +
				this.term +
				"</span>"),
			country = item.country;

		superUl = ul;
		if (item.category == 'place') {
			return $('<li class="search__suggestion">')

				.append(
					'<div class="search__suggestion-wrap">' +
					'<p class="search__suggestion-name">' + newValue + '</p>' +
					'<p class="search__suggestion-province">' + item.province + '</p>' +
					'<p class="search__suggestion-country">' + country + '</p>' +
					'</div>')
				.append('<div class="search__suggestion-excursions">' + item.excursions + '</div>')

				.appendTo(ul);
		}
	},
	_renderMenu: function(ul, items) {
		var that = this;

		$.each( items, function( index, item ) {
			var li;
			li = that._renderItemData( ul, item );
			if ( item.category ) {
				li.attr( "aria-label", item.category + " : " + item.value );
			}
		});
		$(ul).addClass("search__suggestions");
	}
});

	$searchField.catcomplete({
		minLength: 0,
		source: projects,
		appendTo: "#search-results-container",
	});
})();

