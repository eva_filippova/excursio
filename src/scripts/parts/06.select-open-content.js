(function() {
	var $content = $('.js-testimonials-content-item'),
		$select = $('#testimonials-select'),
		$tabs = $('.js-testimonials-tab-item');

	$select.on('change', function() {
		var $t = $(this),
			selectValue = $t.val(),
			$currentContent = $('#testimonials-content-item_' + selectValue);

		$content.removeClass('testimonials__content-item_active');
		$currentContent.addClass('testimonials__content-item_active');

		$currentContent.find('.swiper-container')[0].swiper.update();
		$('.js-testimonial-desc').trunk8();
	});

	$tabs.click(function() {
		var $t = $(this),
			id = $t.data('id'),
			$currentContent = $('#testimonials-content-item_' + id);

		$tabs.removeClass('testimonials__tabs-item_active');
		$t.addClass('testimonials__tabs-item_active');
		$content.removeClass('testimonials__content-item_active');
		$currentContent.addClass('testimonials__content-item_active');

		$currentContent.find('.swiper-container')[0].swiper.update();
		$('.js-testimonial-desc').trunk8();

		return false;
	});
})();